/*
  Exception: error handling ~ controll error on our code
*/

void main() {
  // exception may be occure when dividing a number by 0
  // var a = 10;
  // var b = 0;
  // print(a ~/ b); // exception

  // solution 1
  try {
    var a = 10;
    var b = 0;
    print(a ~/ b);
  } on IntegerDivisionByZeroException {
    print('You can\'t divide a number by 0');
  }

  // solution 2
  try {
    var a = 10;
    var b = 0;
    print(a ~/ b);
  } catch (e, s) {
    print("Exception: $e"); // exception: info
    print("Stack trace: $s"); // stack trace: info
  }

  // solution 3
  try {
    var a = 10;
    var b = 0;
    print(a ~/ b);
  } catch (e, s) {
    print("Exception: $e"); // exception: info
    print("Stack trace: $s"); // stack trace: info
  } finally {
    print("You app is still running");
  }

  // this concept will be useful in development
  // this exception will not crashed our app even it's happen
}
