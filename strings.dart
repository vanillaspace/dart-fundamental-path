/*
  String: data type that contain a single word or a sentence
  string can be define with single quote ('') or double quote("")
*/

void main() {
  String strOne = 'one'; // with single quote
  print(strOne);
  String strTwo = "two"; // with double quote
  print(strTwo);

  // String interpolation: used for put the variable and expression in a string without concatenation.
  // String interpolation use with $ sign for variable and ${} for expression
  print('2 x 5 = ${2 * 5}');

  // add a unicode on a string
  print('I \u2665 Dart');
}
