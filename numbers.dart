/*
  Numbers: Int (Real number) and Double (Decimal number)
*/

void main() {
  // Integer
  var n1 = 20; // type inference
  print(n1);
  int n2 = 50; // explicit type
  print(n2);

  // Double
  var d1 = 2.56; // type inference
  print(d1);
  double d2 = 3.14159; // explicit type
  print(d2);

  // convert to String
  print(n1.toString());
  print(d1.toString());
  // toStrungAsFixed(fraction) generate decimal value with precission
  print(d2.toStringAsFixed(2));
}
