/*
  Operators in Dart
*/

void main() {
  var n1 = 5;
  var n2 = 10;

  // arithmetics operators (+, -, *, /, ~/, %)
  var sum = n1 + n2;
  print(sum);
  var sub = n1 - n2;
  print(sub);
  var multi = n1 * n2;
  print(multi);
  var div = n1 / n2;
  print(div);
  var intDiv = n1 ~/ n2;
  print(intDiv);
  var mod = n1 % n2;
  print(mod);

  // increment & decrement (i++, i--)
  var inc = n1++;
  print(inc);
  var dec = n1--;
  print(dec);

  // comparation (==, !=, >, <, >=, <=)
  // return boolean value
  var isEquals = n1 == n2;
  print(isEquals);
  var isGreaterThan = n1 > n2;
  print(isGreaterThan);

  // logical (&&, ||, !)
  var example = n1 % 2 == 1 && n2 % 2 == 1;
  print(example);
  var notExample = !example;
  print(notExample);
}
