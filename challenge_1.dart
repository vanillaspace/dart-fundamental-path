/*
  Simple CLI App: Temprature Convertion
  Fahrenheit -> Celsius
*/
import 'dart:io';

void main() {
  // input
  stdout.write("Enter temperature (Fahrenheit): ");
  num t_fahrenheit = num.parse(stdin.readLineSync());

  // calculate
  num t_celsius = ((t_fahrenheit - 32) * 5) / 9;

  // output
  print("$t_fahrenheit \u2109 = ${t_celsius.toStringAsFixed(2)} \u2103");
}
