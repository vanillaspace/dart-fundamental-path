/*
  Boolean: Only have 2 possible value, true or false
  used for make a logical expression
*/

void main() {
  // type inference
  var notTrue = !true;
  print(notTrue);
  var notFalse = !false;
  print(notFalse);
  // explicit type
  bool itsTrue = true;
  print(itsTrue);
  bool itsFalse = false;
  print(itsFalse);
}
