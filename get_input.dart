// this is input/output built-in library in dart
import 'dart:io';

void main() {
  // input
  print('Your name: ');
  String name = stdin.readLineSync();
  print('You age: ');
  int age = int.parse(stdin.readLineSync());

  // output
  print('Halo, my name is $name, I am $age years old');
}
