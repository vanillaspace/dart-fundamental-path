// Comments: Used for documentation of our code

void main() {
  // Single line comment

  /*
    multi
    line
    comment
  */

  /// Documentation comment

  // Print the sentence Hello from dart on console
  print('Hello from Dart');

  // Testing documentation comment with [].
  print('5 * 5 = ${5 * 5}');
}
