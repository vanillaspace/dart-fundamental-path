// Variables is used for storing any value to use it later
void main() {
  // to make a variable, we user keyword var
  // Equal sign (=) are assignment operator
  // Assigning a value to a variable called initialization
  var greeting = "Hi there!";
  print(greeting);

  var favoriteNumber; // this is declaration
  favoriteNumber = 15; // this is assignment
  print(favoriteNumber);
}
