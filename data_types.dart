// Data type: To determine what kind of value can be store in a variable

void main() {
  // Type inference
  // when we declare a variable with type inference, it is automatically have dynamic type
  var fruit = "Apple";
  print(fruit);
  var age = 20;
  print(age);

  // Explicit data type
  String word = "Keyboard";
  print(word);
  int price = 50;
  print(price);
  dynamic any = "Ten";
  print(any);
  any = 10;
  print(any);
}
