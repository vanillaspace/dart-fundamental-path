# Dart Fundamental Path

This repository is dart fundamental code for learning

```
Author: Aditya Rohman
Youtube: 'https://www.youtube.com/c/VanillaSpace'
```

To run this code, you need to install the Dart SDK first on your computer.

## Windows

Install

```
choco install dart-sdk
```

Upgrade

```
choco upgrade dart-sdk
```

## Linux

One-Time Setup

```
sudo apt-get update
sudo apt-get install apt-transport-https
sudo sh -c 'wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -'
sudo sh -c 'wget -qO- https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'
```

Install

```
sudo apt-get update
sudo apt-get install dart
```

Modify yout PATH

```
export PATH="$PATH:/usr/lib/dart/bin"
```

## MacOS

Install

```
brew tap dart-lang/dart
brew install dart
```

Upgrade

```
brew upgrade dart
```

Used it for learning and resume your concepts
Thank you